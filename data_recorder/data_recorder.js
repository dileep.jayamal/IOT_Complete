var mqtt = require('mqtt'); //includes mqtt server 
var mongodb = require('mongodb'); // includes mongoDB 
var mongodbClient = mongodb.MongoClient; //initialises the mongoDB client
var mongodbURI = 'mongodb://192.168.1.26:27017/testDB'; //activating the MongoDB port 27017, here TempMontor is the name of the database
var deviceRoot = ""; //deviceroot is topic name given in arduino code 
var collection, client; //initialise collection and client
var db;

Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear() + "-",
    (mm > 9 ? '' : '0') + mm + "-",
    (dd > 9 ? '' : '0') + dd
    ].join('');
};


mongodbClient.connect(mongodbURI, setupCollection); //connect the database with collecion

function setupCollection(err, db) {
    if (err) throw err;
    client = mqtt.connect({ host: '192.168.1.50', port: 1883 }); //connecting the mqtt server with the MongoDB database
    //client.subscribe(deviceRoot+"+"); //subscribing to the topic name 
    client.subscribe("+");
    mongodb=db;
    client.on('message', insertEvent); //inserting the event
}

//function that displays the data in the MongoDataBase
function insertEvent(topic, message) {
    console.log(topic + ': ' + message);
    //var key = topic.replace(deviceRoot, '');
    //console.log(mongodb);
    collection = mongodb.collection(topic); //name of the collection in the database
    
    var json_msg = JSON.parse(message);
    //console.log(json_msg);
    var ts = new Date(json_msg.date_time);
    console.log(json_msg.date_time);
    //var hourstamp = new Date(ts.yyyymmdd() + "T" + ts.getHours() + ":00:00Z");

    var hourstamp = new Date(ts.yyyymmdd() + "T" + ts.getHours() + ":00:00Z");
    //console.log(ts.yyyymmdd());
    //console.log(ts.yyyymmdd() + " " + ts.getHours() + ":00:00");
   
    collection.update(      //timestamp appending
        { _id: hourstamp },
        {
            $push: {
                date_time: ts,
            }
        },
        { upsert: true },

        function (err, docs) {
            if (err) {
                console.log("Insert fail")// Improve error handling		
            }
        }
    );

    collection.update(  // temperature appending
        { _id: hourstamp },
        {
            $push: {
                temperature: json_msg.temperature,
            }
        },
        { upsert: true },

        function (err, docs) {
            if (err) {
                console.log("Insert fail")// Improve error handling		
            }
        }
    );

    collection.update(  //humidity appending
        { _id: hourstamp },
        {
            $push: {
                humidity: json_msg.humidity,
            }
        },
        { upsert: true },

        function (err, docs) {
            if (err) {
                console.log("Insert fail")// Improve error handling		
            }
        }
    );

    collection.update(  //intensity appending
        { _id: hourstamp },
        {
            $push: {
                intensity: json_msg.intensity,
            }
        },
        { upsert: true },

        function (err, docs) {
            if (err) {
                console.log("Insert fail")// Improve error handling		
            }
        }
    );

}
